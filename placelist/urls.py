from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
admin.autodiscover()
print settings.STATIC_ROOT
urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^static/static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    url(r'^places/', include('place.urls')),
    url(r'^$', "place.views.place_list_view"),
)
