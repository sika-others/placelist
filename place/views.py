from django.http import HttpResponse
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from models import Place, Subcategory
from django.shortcuts import get_object_or_404

def place_list_view(request):
    return HttpResponse("200")

class PlaceListView(ListView):
    model = Place

    def get_queryset(self):
        subcategory = get_object_or_404(Subcategory, name__iexact=self.kwargs['slug'])
        return Place.objects.filter(subcategory=subcategory).order_by('subcategory').all()

class PlaceDetailView(DetailView):
    model = Place