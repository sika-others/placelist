from django.db import models

class Subcategory(models.Model):
    name = models.CharField(max_length=250)

    def __unicode__(self):
        return self.name

class Place(models.Model):
    id_hash = models.CharField(max_length=50)
    name = models.CharField(max_length=250)
    slug = models.SlugField(max_length=100, blank=True, null=True)
    perex = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True,)
    profile_picture = models.CharField(null=True, blank=True,max_length=255)
    subcategory = models.ForeignKey(Subcategory)
    
    def __unicode__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('place_detail', (), {'slug': self.slug})
