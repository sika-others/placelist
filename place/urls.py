from django.conf.urls import patterns, url
from views import PlaceListView, PlaceDetailView
from django.views.generic.list import ListView

urlpatterns = patterns('',
    url(r'^browse/(?P<slug>[-\w\d]+)/$', PlaceListView.as_view(), name="place_list"),
    url(r'^(?P<slug>[-\w\d]+)/$', PlaceDetailView.as_view(), name='place_detail'), # no template yet
)
